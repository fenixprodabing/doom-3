cd ..
del /s /q build
mkdir build
cd build
mkdir debug
cd debug
cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../../bin/win32 -DDHEWM3LIBS=../../libs/x86 ../../neo
cd ..
mkdir release
cd release
cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../bin/win32 -DDHEWM3LIBS=../../libs/x86 ../../neo
pause